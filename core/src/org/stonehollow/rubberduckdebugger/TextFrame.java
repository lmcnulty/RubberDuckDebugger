package org.stonehollow.rubberduckdebugger;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import java.util.Stack;
import static com.badlogic.gdx.utils.TimeUtils.nanoTime;

/**
 * Created by luna on 6/3/17.
 */

public class TextFrame {

    Boolean visible;

    NinePatch body;
    Texture image;
    Vector2 position;
    Vector2 dimensions;

    String text;
    String[] words;

    BitmapFont font;
    int frameBorder;
    float textPadding;

    int wordAt;

    float printDuration;
    long printStartTime;
    boolean printFinished;
    Stack<String> printStack;
    GlyphLayout glyphLayout;
    GlyphLayout glyphLayout2;


    public TextFrame(){
        visible = true;
        font = new BitmapFont(Gdx.files.internal("cantarell.fnt"),false);
        font.setColor(Color.valueOf("#3C3C3C"));
        font.getData().setScale(2f);
        font.setUseIntegerPositions(false);
        glyphLayout = new GlyphLayout();
        glyphLayout2 = new GlyphLayout();
        printFinished = false;
        printStack = new Stack<String>();
        position = new Vector2(0,0);
        dimensions = new Vector2(0,0);
        printDuration = 50;
        printStartTime = nanoTime();
        text = "";
        this.print("");
        image = new Texture(Gdx.files.internal("textbox.png"));
        frameBorder = 72;
        textPadding = 100;
        int fb = frameBorder;
        body = new NinePatch(image,fb,fb,fb,fb);
    }
    public void print(String someString){
        printStartTime = nanoTime();
        text = someString;
        words = text.split(" ");
        printFinished = false;
    }
    public void render(SpriteBatch batch, float delta){
        if(visible) {
            body.draw(batch, position.x, position.y, dimensions.x, dimensions.y);
            int maxNumberOfLines = (int) ((dimensions.y - 2 * textPadding) / font.getLineHeight());
            int linesPrinted = 0;
            wordAt = 0;
            while (linesPrinted < maxNumberOfLines && wordAt < words.length) {
                String partialText = "";
                glyphLayout.setText(font, partialText);
                glyphLayout2.setText(font, words[wordAt] + " ");

                while (glyphLayout.width + glyphLayout2.width < dimensions.x - 2 * (frameBorder + textPadding) && wordAt < words.length) {

                    partialText += words[wordAt] + " ";
                    glyphLayout.setText(font, partialText);
                    glyphLayout2.setText(font, words[wordAt] + " ");
                    wordAt += 1;
                }
                font.draw(batch, partialText, position.x + frameBorder + textPadding, position.y + dimensions.y - frameBorder - textPadding - (linesPrinted * font.getLineHeight()));
                linesPrinted += 1;
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
                this.proceed();
            }
            if (wordAt >= words.length) {
                printFinished = true;
            }
        }

    }
    public void proceed(){
        if (wordAt < words.length){
            String[] newWords = new String[words.length-wordAt];
            for(int i=0;i<words.length-wordAt;i++){
                newWords[i] = words[i+wordAt];
            }
            words = newWords;
            wordAt = 0;
        }
    }
}
